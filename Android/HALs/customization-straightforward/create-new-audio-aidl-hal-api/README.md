# Create new Audio AIDL HAL API

{% hint style="danger" %}
This ussually is not necessary for OEMs, except you are trying to develop a new Android version
{% endhint %}

## Create a new version of the audio HAL

To create a new version of the audio HAL based on an existing one, run:

```
hardware/interfaces/audio/common/all-versions/copyHAL.sh
```
