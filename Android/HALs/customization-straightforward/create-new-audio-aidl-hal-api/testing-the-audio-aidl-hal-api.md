# Testing the Audio AIDL HAL API

New [VTS](https://android.googlesource.com/platform/hardware/interfaces/+/refs/heads/main/audio/aidl/vts/) tests are provided for the AIDL interface.

There are no changes in the new HAL version that can affect security.
