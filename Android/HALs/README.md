---
description: List all HALs
---

# List all HALs

List all HALs

```sh
/system/xbin/su root lshal --debug
```
