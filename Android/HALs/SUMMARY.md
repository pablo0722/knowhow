# Table of contents

## Commands

* [List all HALs](README.md)

## Information

* [Module automatic load](information/module-automatic-load.md)
* [Default implementation](information/default-implementation/README.md)
  * [TinyHal](information/default-implementation/tinyhal.md)
  * [Default](information/default-implementation/default.md)
* [HIDL](information/hidl/README.md)
  * [HIDL Audio HAL](information/hidl/hidl-audio-hal.md)
* [AIDL](information/aidl/README.md)
  * [AIDL Audio HAL](information/aidl/aidl-audio-hal/README.md)
    * [Interface Definition](information/aidl/aidl-audio-hal/interface-definition.md)
    * [Interface Relation](information/aidl/aidl-audio-hal/interface-relation.md)

## Customization Straightforward

* [Create new Audio AIDL HAL API](customization-straightforward/create-new-audio-aidl-hal-api/README.md)
  * [Testing the Audio AIDL HAL API](customization-straightforward/create-new-audio-aidl-hal-api/testing-the-audio-aidl-hal-api.md)
* [Create custom HIDL-based Audio Hal](customization-straightforward/create-custom-hidl-based-audio-hal.md)
* [Create custom AIDL-based Audio HAL](customization-straightforward/create-custom-aidl-based-audio-hal.md)
