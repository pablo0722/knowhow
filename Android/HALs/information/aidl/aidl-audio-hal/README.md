# AIDL Audio HAL

## Overview

Extending the `libaudiohal` library adds framework support for AIDL HAL.

Switching to HIDL to AIDL and deprecating and removing support for previous major Audio HAL versions frees up disk space and RAM on devices. This leads to a smoother UX and allows for newer user-visible features for end users.

The Audio Policy Manager (APM) gets the configuration from the HAL instead of consuming it from the vendor-provided XML file.

Default implementation is at:

```
hardware/interfaces/audio/aidl/default/
```

Streams are unidirectional and are used by AudioFlinger to send or receive audio to and from the HAL.

#### Audio HAL API <a href="#audiohal-api" id="audiohal-api"></a>

The Audio HAL contains the following APIs:

* The Core HAL is the main API used by AudioFlinger to play audio and control the audio routing.
*   The Effects HAL API is used by the effects framework to control audio effects. You can also configure [preprocessing effects](https://source.android.com/devices/audio/implement-pre-processing) such as automatic gain control and noise suppression through the Effects HAL API.

    **Note:** The Audio Effects HAL API is similar to the [framework effects APIs](https://developer.android.com/reference/android/media/audiofx/AudioEffect).
* The Common HAL API is a library of common data types used by the Core and Effects HAL APIs. The Common HAL has no interfaces and no associated VTS tests as it defines only data structures.

See [AIDL Audio HAL](https://source.android.com/docs/core/audio/aidl-implement) and [HIDL Audio HAL](https://source.android.com/docs/core/audio/hidl-implement) for specific details related to AIDL and HIDL respectively.

## Existing versions

* Starting with Android 13, Audio HAL is updated to version 7.1.

## Requirements <a href="#requirements" id="requirements"></a>

In addition to implementing the Audio HAL and creating the audio policy configuration file, you must adhere to the following HAL requirements:

*   If capture for Sound Trigger (capture from hotword DSP buffer) is supported by one input profile, the implementation must support the number of active streams on this profile corresponding to the number of concurrent sessions supported by Sound Trigger HAL.

    **Note:** The implementation of the [Sound trigger HAL](https://source.android.com/docs/core/audio/sound-trigger) is located at `/hardware/interfaces/soundtrigger` and is coupled with the Audio HAL.

## Files Location

* AIDL HAL interface: The Audio HAL interface is defined using AIDL (Android Interface Definition Language) in AIDL HAL files. These files are located in the /hardware/interfaces/audio/aidl/aidl\_api directory.
* AIDL HAL implementation: The AIDL HAL implementation is provided by the device manufacturer and is typically located in the /vendor/\<vendor>/\<device>/bin directory.
* JNI headers: The JNI headers for the Audio HAL are located in the /frameworks/base/core/jni and /frameworks/base/media/jni directories.
* Native framework: The native framework provides a native equivalent to the Android audio APIs. This framework is located in the /frameworks/av/services/audioflinger directory.
* Media server: The media server is the main process responsible for managing audio playback and recording on Android devices. It is located in the /frameworks/av/mediaserver directory.

In addition to these locations, the Android audio HAL may also be present in other parts of the system, such as the audio mixer and the sound trigger HAL.
