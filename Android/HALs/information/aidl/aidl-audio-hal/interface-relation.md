# Interface Relation

## Relation Tree

1. Definition of `aidl_interface` lower layer:
   * file location:
     * `hardware/interfaces/audio/aidl/Android.bp:`
   * file content:
     * ```
         aidl_interface {
             name: "android.hardware.audio.core",
             srcs: [
                 "android/hardware/audio/core/*.aidl",
             ],
             ...
         }
       ```
2. Definition of `aidl_interface` higher layer:
   * file location:
     * `frameworks/av/Android.bp:`
   * file content:
     * ```
         aidl_interface {
             name: "av-audio-types-aidl",
             host_supported: true,
             vendor_available: true,
             srcs: [
                 ".../IHalAdapterVendorExtension.aidl",
             ],
             imports: [
                 "android.hardware.audio.core-V1",
             ],
             ...
         }
       ```
3. HAL implementation lower layer:
   * Info:
     * This is the implementation that Vendors should do.
     * You can use TinyHal implementation as example (uses HIDL).
   * file location:
     * `external/tinyhal/audio/Android.mk`
   * file content:
     * ```
         LOCAL_MODULE := audio.primary.$(TARGET_DEVICE)
         LOCAL_SRC_FILES := audio_hw.c
       ```
4. HAL implementation mid layer:
   * file location:
     * `frameworks/av/media/libaudiohal/impl/Android.bp`
   * file content:
     * ```
         cc_library_shared {
             name: "libaudiohal@aidl",
             shared_libs: [
                 "av-audio-types-aidl-ndk",
                 ...
             ],
             srcs: [
                 "*.cpp",
                 ":core_audio_hal_aidl_src_files",
             ],
             ...
         }

         filegroup {
             name: "core_audio_hal_aidl_src_files",
             srcs: [
                 "ConversionHelperAidl.cpp",
                 "DeviceHalAidl.cpp",
                 "DevicesFactoryHalAidl.cpp",
                 "StreamHalAidl.cpp",
             ],
         }
       ```
5. HAL implementation higher layer:
   * file location:
     * `frameworks/av/media/libaudiohal/Android.bp`
   * file content:
     * ```
         cc_library_shared {
             name: "libaudiohal",
             srcs: [
                 "DevicesFactoryHalInterface.cpp",
                 "EffectsFactoryHalInterface.cpp",
                 "FactoryHal.cpp",
             ],
             required: [
                 "libaudiohal@4.0",
                 "libaudiohal@5.0",
                 "libaudiohal@6.0",
                 "libaudiohal@7.0",
                 "libaudiohal@7.1",
                 "libaudiohal@aidl",
             ],
             ...
         }
       ```

## Relation Graph

<img src="../../../.gitbook/assets/file.excalidraw.svg" alt="" class="gitbook-drawing">
