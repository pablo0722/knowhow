# Interface Definition

## Interface directory structure

See the audio interface directory structure at:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/README.md" %}

## Interface definitions

### Core AIDLs

Last version on development:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core" %}

Last release version:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/aidl_api/android.hardware.audio.core/" %}

* [`IModule.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/IModule.aidl) is the entry point into the API.
* Streams are unidirectional and are used by AudioFlinger to send or receive audio to and from the HAL through [`IStreamOut.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/IStreamOut.aidl) and [`IStreamIn.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/IStreamIn.aidl).
* [`ITelephony.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/ITelephony.aidl) provides controls specific to telephony functions.
* [`IBluetooth.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/IBluetooth.aidl) provides BT SCO and HFP controls that were on `IPrimaryModule` in the HIDL API.
* [`IConfig.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/IConfig.aidl) provides system-wide configuration parameters.
* [`ISoundDose.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/sounddose/ISoundDose.aidl) provides sound dose support. See [Sound dose](https://source.android.com/docs/core/audio/sound-dose) for more information.
* The latest version of the Core HAL API _in development_ is in [`/hardware/interfaces/audio/aidl/android/hardware/audio/core/`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/core/).
* The latest _released_ version of the Core HAL API is in [`/hardware/interfaces/audio/aidl/aidl_api/android.hardware.audio.core/`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/aidl\_api/android.hardware.audio.core/).

### Effect AILDs

Last version on development:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/" %}

Last release version:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/aidl_api/android.hardware.audio.effect/" %}

* [`IFactory.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/IFactory.aidl) is the entry point into the API.
* [`Descriptor.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/Descriptor.aidl) contains all information such as capabilities and attributes for an effect implementation.
* [`Capability.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/Capability.aidl) defines effect capabilities that don't change at runtime.
* [`Parameter.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/Parameter.aidl) defines all parameters supported by the effect instance.
* [`IEffect.aidl`](https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/effect/IEffect.aidl) is used to configure and control particular effect instances.
* Effect-specific parcelables named after the effect.

See the [Audio Effects](https://source.android.com/docs/core/audio/audio-effects) for more information.

## Common HAL AILDs

Data structures and interfaces shared between various HALs such as BT HAL, core and effects Audio HALs are in the Common HAL.

Last version on development:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/android/hardware/audio/common/" %}

Last release version:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:hardware/interfaces/audio/aidl/aidl_api/android.hardware.audio.common/" %}

### **Common stable data types**

Stable data structure definitions are used both by HALs and the framework.

Last version on development:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:system/hardware/interfaces/media/aidl/android/media/audio/common/" %}

Last release version:

{% embed url="https://cs.android.com/android/platform/superproject/+/main:system/hardware/interfaces/media/aidl_api/android.media.audio.common.types/" %}
