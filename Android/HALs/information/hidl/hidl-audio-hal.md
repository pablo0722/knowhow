# HIDL Audio HAL

## Overview

The Audio Policy Manager (APM) gets the configuration from a vendor-provided XML file.

Streams are unidirectional and are used by AudioFlinger to send or receive audio to and from the HAL

## Files Location

* HIDL HAL files: The Audio HAL interface is defined using HIDL (Hardware Interface Definition Language) in HIDL HAL files (with the extension .xml). These files are located in the /hardware/interfaces/audio directory.
* Implementation HAL files: The implementation of the Audio HAL is provided by the device manufacturer and is typically located in the /vendor/\<vendor>/\<device>/drivers/audio directory.
* JNI headers: The JNI headers for the Audio HAL are located in the /frameworks/base/core/jni and /frameworks/base/media/jni directories.
* Native framework: The native framework provides a native equivalent to the Android audio APIs. This framework is located in the /frameworks/av/services/audioflinger directory.
* Media server: The media server is the main process responsible for managing audio playback and recording on Android devices. It is located in the /frameworks/av/mediaserver directory.

In addition to these locations, the Android audio HAL may also be present in other parts of the system, such as the audio mixer and the sound trigger HAL.
