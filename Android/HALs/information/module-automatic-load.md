# Module automatic load

### Load Audio Module

Android tries to load automatically an Audio module following these steps:

1. First tries to get property `ro.hardware.audio.primary`, if exists, tries to load `audio.primary.<ro.hardware.audio.primary>.so` module.
2. If fails, tries to get property `ro.hardware`, if exists, tries to load `audio.primary.<ro.hardware>.so"`module.
3. If fails, tries to load `audio.primary.default.so` module.

_Note_: To see how Android sets `ro.hardware` property, see [System Properties](https://app.gitbook.com/s/EKeNTTt1uY7SM7jYpR5l/information/system-properties "mention")

The method `hw_get_module_by_class` is the one that follows the previous steps, and is located at `hardware/libhardware/hardware.c`

Method `hw_get_module_by_class` is called by `loadAudioInterface`.

The method `loadAudioInterface` is the one that tries to load a module, and is located at `hardware/interfaces/audio/core/all-versions/default/DevicesFactory.cpp`

#### **Snippet:**

```c
loadAudioInterface() { // hardware/interfaces/audio/core/all-versions/default/DevicesFactory.cpp
    module = hw_get_module_by_class(class_id = "audio", if_name = "primary") { // hardware/libhardware/hardware.c
        if(preperty_exist("ro.hardware.audio.primary")) {
            prop = property_get("ro.hardware.audio.primary");
            lib_name = `${class_id}.${if_name}.${prop}.so`;
        } else if(preperty_exist("ro.hardware")) {
            prop = property_get("ro.hardware");
            lib_name = `${class_id}.${if_name}.${prop}.so`;
        } else {
            lib_name = `${class_id}.${if_name}.default.so`;
        }

        path = get_hw_module_path(lib_name);
        return load(class_id, path);
    }
    
    audio_hw_device_open(module);
}
```

