---
description: Get Shared Preference At Runtime
---

# Get Shared Preference At Runtime

Directory of shared preferences:

```sh
ls /data_persist/data/
```

List all shared preference files for a given package `{package}`:

```sh
ls /data_persist/data/{package}/shared_prefs/ | grep \.xml
```

Retrieve information of a shared preference file `{file}` for a given package `{package}`:

```sh
cat /data_persist/data/{package}/shared_prefs/{file}
```
