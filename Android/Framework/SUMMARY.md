# Table of contents

## Commands

* [General](README.md)
  * [Print SDK Version](<README (1).md>)
* [Activities](commands/activities/README.md)
  * [Get Current Activity Window](activities/get-current-activity-window.md)
  * [Get Installed Packages](activities/get-installed-packages.md)
  * [Get Recent Activities](activities/get-recent-activities.md)
  * [Start Activity](activities/start-activity.md)
  * [Remove App](activities/remove-app.md)
* [APK](commands/apk/README.md)
  * [Parse APK](apk/parse-apk.md)
* [Shared Preference](commands/shared-preference/README.md)
  * [Get Shared Preference At Runtime](shared-preference/get-shared-preference-at-runtime.md)

## Information

* [Service-Application binding](information/service-application-binding.md)
* [Activity](information/activity/README.md)
  * [Analysis of Dagger-related code](information/activity/analysis-of-dagger-related-code.md)
