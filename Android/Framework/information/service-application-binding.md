# Service-Application binding

## Starting a service

When satrting a Service, the methods calling order is:

1. The Application instance is created
2. The Application's `onCreate()` method is called
3. The Service instance is created
4. The Service's `onCreate()` method is called

You can check this order in the source code:

{% code title="frameworks/base/core/java/android/app/ActivityThread.java" lineNumbers="true" %}
```java
@UnsupportedAppUsage
private void handleCreateService(CreateServiceData data) {
        // ...

        if (localLOGV) Slog.v(TAG, "Creating service " + data.info.name);

        // The application instance is created first
        Application app = packageInfo.makeApplication(false, mInstrumentation);

        // Then the Service instance is created
        service = packageInfo.getAppFactory()
                .instantiateService(cl, data.info.name, data.intent);

        // Service resources are initialized with the same loaders as the
        // application context.
        context.getResources().addLoaders(
                    app.getResources().getLoaders().toArray(new ResourcesLoader[0]));

        context.setOuterContext(service);
        service.attach(context, this, data.info.name, data.token, app,
                ActivityManager.getService());

        // Finally the Service's onCreate() method is called
        service.onCreate();

        // ...
}
```
{% endcode %}

When the application instance is created (`makeApplication` method), the Application's `onCreate()` is called:

{% code title="frameworks/base/core/java/android/app/LoadedApk.java" lineNumbers="true" %}
```java
@UnsupportedAppUsage
public Application makeApplication(boolean forceDefaultAppClass,
        Instrumentation instrumentation) {
    // ...
    if (instrumentation != null) {
        try {
            instrumentation.callApplicationOnCreate(app);
        } catch (Exception e) {
            // ...
        }
    }
    // ...
}
```
{% endcode %}

{% code title="frameworks/base/core/java/android/app/Instrumentation.java" lineNumbers="true" %}
```java
public void callApplicationOnCreate(Application app) {
    app.onCreate();
}
```
{% endcode %}
