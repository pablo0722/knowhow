---
description: Start Activity
---

# Start Activity

Start Activity `{package/activity}` in format `package/activity`

```sh
am start -n {package/activity}
```

List of activities:

| Activity name       | Package                             | Activity                                                    | {package/activity}                                           |
| ------------------- | ----------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------------ |
| touch\_screen\_test | jp.rallwell.siriuth.touchscreentest | jp.rallwell.siriuth.touchscreentest.TouchScreenTestActivity | jp.rallwell.siriuth.touchscreentest/.TouchScreenTestActivity |
| image\_viewer       | com.not.aa\_image\_viewer           | com.not.aa.StartActivity                                    | com.not.aa\_image\_viewer/com.not.aa.StartActivity           |

{% @quizzes/quizzes %}
