---
description: Get Recent Activities
---

# Get Recent Activities



```sh
am start -n com.android.systemui/com.android.systemui.recents.RecentsActivity
```

```sh
dumpsys activity recents
```

Print stack of recent activities:

```sh
dumpsys activity a | grep -i -E '^      realActivity|^          state='
```
