---
description: Get Installed Packages
---

# Get Installed Packages

```python
pm list packages -f
```
