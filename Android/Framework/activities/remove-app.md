---
description: Remove App
---

# Remove App

Remove a system App `{app}`

```sh
mount -o rw,remount /system
```

```sh
rm -r system/app/{app}
```

```sh
reboot
```
