---
description: Get Current Activity Window
---

# Get Current Activity Window

For Android 8

```sh
dumpsys window windows | grep -E 'mCurrentFocus|mFocusedApp'
```

For android 14

```sh
adb shell dumpsys window | grep "#8"
```
