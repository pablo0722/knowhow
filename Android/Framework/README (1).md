---
description: Print Android SDK version
---

# Print SDK Version

Print Android SDK version

```sh
dumpsys package | grep sdkVersion= -B 2 -A 1
```
