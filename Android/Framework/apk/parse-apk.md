---
description: Parse APK
---

# Parse APK

Replace `{apk_path}` with the path of the APK you want to analyse

{% code overflow="wrap" %}
```sh
prebuilts/sdk/tools/linux/bin/aapt dump badging {apk_path} | grep -iE "package:|launchable-activity"
```
{% endcode %}

{% code overflow="wrap" %}
```sh
prebuilts/sdk/tools/linux/bin/aapt list -a {apk_path} | sed -n 'activity /{:loop n;s/^.*android:name.*="\([^"]\{1,\}\)".*/\1/;T loop;p;t}'
```
{% endcode %}
