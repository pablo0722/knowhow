# Table of contents

## Commands

* [Get mounts](README.md)
* [Remount partitions](commands/remount-partitions.md)

## Information

* [Partitions](information/partitions/README.md)
  * [System](information/partitions/system.md)
  * [Product](information/partitions/product.md)
  * [Device](information/partitions/device.md)
  * [Vendor](information/partitions/vendor.md)

## Customize Straightforward

* [Separate new partitions](customize-straightforward/separate-new-partitions.md)
