# Partitions

## Overview

* Software Partitions
  * `System` is general, `Product` is an extension, has customizations. They are linked by a weak ABI.\
    E.g.: Google developes `System` partition. Customers develops `Product` partition.
  * Both implements:
    * | Component                        | Location                                                            |
      | -------------------------------- | ------------------------------------------------------------------- |
      | system properties                | `/<partition>/build.prop`                                           |
      | Runtime resource overlays (RROs) | `/<partition>/overlay/*.apk`                                        |
      | Apps                             | `/<partition>/app/*.apk`                                            |
      | Priv-apps                        | `/<partition>/priv-app/*.apk`                                       |
      | Libraries                        | `/<partition>/lib/*`                                                |
      | Java libraries                   | `/<partition>/framework/*.jar`                                      |
      | Android Framework system configs | `/<partition>/etc/sysconfig/*` and `/<partition>/etc/permissions/*` |
      | Media files                      | `/product/media/audio/*`                                            |
      | `bootanimation` files            |                                                                     |
      * _Note_: `/<partition>` is either `/system` or `/product`
  * `System Partition`:
    * Stores stock Android system image.
  * `Product Partition`:
    * Stores the customisation to the Android framework.
    * Supplies different software SKUs for each product.
    * Build flags:
      * `product_specific: true` in `Android.bp`
      * `LOCAL_PRODUCT_MODULE := true` in `Android.mk`
* Hardware Partitions
  * `Vendor` is general, `odm` is an extension, has customizations. They are linked by a weak ABI.\
    E.g.: Texas Instruments developes `vendor` partition. Customers develops `odm` partition.
  * Both implements:
    * | Component                                                                                      | Location                                                            |
      | ---------------------------------------------------------------------------------------------- | ------------------------------------------------------------------- |
      | Loadable kernel modules (LKMs)                                                                 | `/<partition>/lib/modules/*.ko`                                     |
      | Native libraries                                                                               | `/<partition>/lib[64]`                                              |
      | HALs                                                                                           | `/<partition>/lib[64]/hw`                                           |
      | SEPolicy                                                                                       | `/<partition>/etc/selinux`                                          |
      | [VINTF object data](https://source.android.com/docs/core/architecture/vintf/objects)           | `/<partition>/etc/vintf`                                            |
      | [`init.rc` files](https://android.googlesource.com/platform/system/core/+/main/init/README.md) | `/<partition>/etc/init`                                             |
      | System properties                                                                              | `/<partition>/build.prop`                                           |
      | Runtime resource overlays (RROs)                                                               | `/<partition>/overlay/*.apk`                                        |
      | Apps                                                                                           | `/<partition>/app/*.apk`                                            |
      | Priv-apps                                                                                      | `/<partition>/priv-app/*.apk`                                       |
      | Java libraries                                                                                 | `/<partition>/framework/*.jar`                                      |
      | Android Framework system configs                                                               | `/<partition>/etc/sysconfig/*` and `/<partition>/etc/permissions/*` |
    * _Note_: `/<partition>` is either `/vendor` or `/odm`
  * Vendor Partition:
    * Generic for multiple devices with the same SoC or the same family of SoC.
    * Contains the Board Support Package (BSPs) of the SoC.
  * `vendor_dlkm partition`:
    * Stores Vendor kernel modules.
    * This makes possible to update kernel modules without updating the `vendor` partition.
  * `Odm Partition`:
    * ODM is for Original Design Manufacturers
    * Supplies different hardware SKUs for each platform.
    * Build flags:
      * `device_specific: true` in `Android.bp`
      * `LOCAL_ODM_MODULE := true` in `Android.mk`
  * `odm_dlkm partition`:
    * Stores ODM kernel modules.
    * This makes possible to update ODM kernel modules without updating the `odm` partition.

_Note_: SKU (Stock Keeping Unit) is a unique identifier for each product (i.e. Product ID).

### Dynamic Partitions

Android 10 support for [dynamic partitions](https://source.android.com/docs/core/ota/dynamic\_partitions) removes the disk issue, and makes repartitioning a device during an [over-the-air (OTA)](https://source.android.com/docs/core/ota) update possible.

