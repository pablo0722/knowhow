---
description: Remount partitions
---

# Remount partitions

```sh
mount -o rw,remount /system
mount -o rw,remount /vendor
```
