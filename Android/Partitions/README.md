---
description: Get mounts from dumpsys
---

# Get mounts

Get mounts from dumpsys

```sh
dumpsys mount
```
