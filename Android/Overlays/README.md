---
description: Get overlays from dumpsys
---

# Get Overlays

Get overlays from dumpsys

```sh
dumpsys overlay
```
