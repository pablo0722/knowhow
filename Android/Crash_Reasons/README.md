---
description: Print last reboot reason
---

# Last Reboot Reason

Print last reboot reason

```sh
cat /data/misc/reboot/last_reboot_reason
```
