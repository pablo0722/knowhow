---
description: Get crashes dump
---

# Get Crashes Dump

Get crashes dump from dumpsys

```sh
/system/bin/dumpsys -t 30 dropbox -p system_server_crash
```
