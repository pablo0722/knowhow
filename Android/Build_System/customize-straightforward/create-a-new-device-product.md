# Create a new device-product

## Overview

To create a new product, see https://wladimir-tm4pda.github.io/porting/build\_new\_device.html

## Downloads

### Simple implementation example

Very easy product + board example.

Util when studying usage.

{% file src="../.gitbook/assets/Create_product_example.7z" %}

### Easy edit placeholder product

Product + board example with placeholders and overlays easy to edit.

Util to use as a base new product.

{% file src="../.gitbook/assets/Easy_edit_placeholder_product.7z" %}

### Simple Product - Multiple Board Example

Product + board example with placeholders and overlays easy to edit and multiple boards.

Util to use as a base new development product looking for a suitable board.

{% file src="../.gitbook/assets/Easy edit placeholder product multi board.7z" %}
