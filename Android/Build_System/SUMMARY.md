# Table of contents

## Information

* [Conditional Compilation](README.md)
* [Device Product Makefile](information/device-product-makefile.md)
* [Android.mk](information/android.mk/README.md)
  * [Filter makefile to run on specific Board](information/android.mk/filter-makefile-to-run-on-specific-board.md)

## Commands

* [Build](commands/build.md)

## Customize StraightForward

* [Create a new device-product](customize-straightforward/create-a-new-device-product.md)
