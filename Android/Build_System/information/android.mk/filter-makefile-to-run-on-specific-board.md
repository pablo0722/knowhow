# Filter makefile to run on specific Board

If you add an `Android.mk` in AOSP, it will be built on ALL platforms, including x86, the emulator, and the SDK. Modules must be uniquely named (liblights.panda), and must build everywhere, or limit themselves to only building on, e.g. ARM, if they include assembly. Individual makefiles are responsible for having their own logic, for fine-grained control.

To limit an Android.mk to only build for a specific board:

{% code title="Android.mk" lineNumbers="true" %}
```makefile
# MyBoardName is the same set on $(PRODUCT_DEVICE)
ifneq ($(filter MyBoardName%, $(TARGET_DEVICE)),)
    ...
endif
```
{% endcode %}
