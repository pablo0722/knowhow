# Device Product Makefile

## Notes

* Product Makefile (the one listed in the `AndroidProducts.mk` as `PRODUCT_MAKEFILES`):
  * Makefile's name must be the same as Lunch contained in `COMMON_LUNCH_CHOICES` on `AndroidProducts.mk` file in format \<filename>-$(TARGET\_BUILD\_VARIANT).
  *   File content:

      <table><thead><tr><th width="253">Variable</th><th>Description</th><th>Example</th><th>Required/Optional</th></tr></thead><tbody><tr><td><code>PRODUCT_NAME</code></td><td>End-user-visible name for the overall product. Appears in the <strong>Settings > About</strong> screen.</td><td><code>XC40</code></td><td>Required</td></tr><tr><td><code>PRODUCT_DEVICE</code></td><td>Name of the industrial design. This is also the board name, and the build system uses it to locate <code>BoardConfig.mk</code>.</td><td><code>BeaglePlay</code></td><td>Required</td></tr><tr><td><code>PRODUCT_BRAND</code></td><td>The brand the software is customized for.</td><td><code>Ferrari</code></td><td>Optional</td></tr><tr><td><code>PRODUCT_MODEL</code></td><td>End-user-visible name for the end product.</td><td><code>AOSP Car on BeaglePlay</code></td><td>Optional</td></tr><tr><td><code>PRODUCT_MANUFACTURER</code></td><td>Name of the manufacturer.</td><td><code>Panasonic</code></td><td>Optional</td></tr><tr><td><code>PRODUCT_LOCALES</code></td><td>A space-separated list of two-letter language code, two-letter country code pairs that describe several settings for the user, such as the UI language and time, date, and currency formatting. The first locale listed in <code>PRODUCT_LOCALES</code> is used as the product's default locale.</td><td><code>en_GB</code>, <code>de_DE</code>, <code>es_ES</code>, <code>fr_CA</code></td><td>Optional</td></tr></tbody></table>
* BoardConfig.mk:
  * Must be inside a directory with the same name as Device contained in `PRODUCT_DEVICE` on Product Makefile file.
  *   File content:

      <table><thead><tr><th width="244">Variable</th><th>Description</th><th width="163">Example</th><th>Required/Optional</th></tr></thead><tbody><tr><td><code>TARGET_ARCH</code></td><td></td><td><code>arm</code></td><td>Required</td></tr><tr><td><code>TARGET_ARCH_VARIANT</code></td><td></td><td><code>armv8-a</code></td><td>Required</td></tr><tr><td><code>TARGET_CPU_ABI</code></td><td></td><td><code>armeabi-v7a</code></td><td>Required</td></tr><tr><td>TARGET_CPU_VARIANT</td><td></td><td><code>cortex-a53</code></td><td>Optional</td></tr><tr><td>TARGET_CPU_ABI2</td><td></td><td><code>armeabi</code></td><td>Optional</td></tr></tbody></table>
