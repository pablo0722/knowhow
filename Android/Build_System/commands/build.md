# Build

## Force Build of current directory

Touch recursively every file of current directory with:

```sh
find . -type f -name "*" -exec touch {} +
```

Build current directory:

```sh
mm
```
