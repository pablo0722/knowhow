---
description: Information about Audio Zones
---

# Audio Zone

`car_audio_configuration.xml` contains the volume Zone information.

possible paths of this file on source are:

* `device/generic/car/emulator/audio/car_audio_configuration.xml`

possible paths of this file on target are:

* `/system/etc/car_audio_configuration.xml`
* `/vendor/etc/car_audio_configuration.xml`

Example of content of `car_audio_configuration.xml` :

{% code title="car_audio_configuration.xml" lineNumbers="true" %}
```xml
<!--
  Defines the audio configuration in a car, including
    - Audio zones
    - Context to audio bus mappings
    - Volume groups
  in the car environment.
-->
<carAudioConfiguration version="2">
    <zones>
        <zone name="primary zone" isPrimary="true" occupantZoneId="0">
            <volumeGroups>
                <group>
                    <device address="bus0_media_out">
                        <context context="music"/>
                        ...
                    </device>
                    ...
                </group>
                ...
            </volumeGroups>
        </zone>
        <zone name="rear seat zone 1" audioZoneId="1">
            ...
        </zone>
        ...
    </zones>
</carAudioConfiguration>
```
{% endcode %}
