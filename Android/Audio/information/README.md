---
description: Information about Audio framework on Android 14
---

# Information

Each Audio Zone contains 1 or more Volume Groups.

Each Volume Group contains 1 or more Devices.

Each Device contains 1 or more Context.

When setting volume, the volume value is setted to each device in a Volume Group.
