---
description: Information about Volume Groups
---

# Volume Group

`car_volume_groups.xml` contains the volume group information.

Example of content of `car_volume_groups.xml` :

{% code title="car_volume_groups.xml" lineNumbers="true" %}
```xml
<volumeGroups>
    <group>
        <context car:context="music"/>
        <context car:context="call_ring"/>
        ...
    </group>
    ...
</volumeGroups>
```
{% endcode %}
