---
description: Information about Dynamic Audio Routing
---

# Dynamic Audio Routing

## Get/Set Volume

There are methods to get/set the Volume of a Volume Group of an Audio zone, including its minimum, maximum and current values. These operations are done in the `CarAudioManager` which then calls `CarAudioService`:

{% code title="CarAudioManager.java" lineNumbers="true" %}
```java
@SystemApi
@RequiresPermission(Car.PERMISSION_CAR_CONTROL_AUDIO_VOLUME)
public void setGroupVolume(int zoneId, int groupId, int index, int flags)

@SystemApi
@RequiresPermission(Car.PERMISSION_CAR_CONTROL_AUDIO_VOLUME)
public int getGroupMaxVolume(int zoneId, int groupId)

@SystemApi
@RequiresPermission(Car.PERMISSION_CAR_CONTROL_AUDIO_VOLUME)
public int getGroupMinVolume(int zoneId, int groupId)

@SystemApi
@RequiresPermission(Car.PERMISSION_CAR_CONTROL_AUDIO_VOLUME)
public int getGroupVolume(int zoneId, int groupId)
```
{% endcode %}

* When dynamic routing is NOT used
  * There is no concept of vehicle audio zones, and volume information is not maintained in `CarAudioService`. These methods map Volume Groups to STREAM\_TYPE and get/set the corresponding volume from/to `AudioManager`.
* When using dynamic routing
  * The volume information is maintained in `CarAudioService`. These methods get/set the volume group information based on the zone ID and volume group ID, and get/set the corresponding volume from/to the Volume Group `packages/services/Car/service/src/com/android/car/audio/CarVolumeGroup.java`:

The maximum, minimum, step value, etc, ​​of the Volume Group of the Audio Zone, comes from the device.

The method to get  the volume value from Settings is done in `packages/services/Car/service/src/com/android/car/audio/CarAudioSettings.java`:

{% code title="CarAudioSettings.java" lineNumbers="true" %}
```java
int getStoredVolumeGainIndexForUser(int userId, int zoneId, int groupId) {
    return Settings.System.getIntForUser(mContentResolver,
            getVolumeSettingsKeyForGroup(zoneId, groupId), -1, userId);
}
```
{% endcode %}
