---
description: Audio diagnosis commands
---

# Diagnosis commands

## Dumpsys

Audio diagnosis commands

```
dumpsys audio
```

```
dumpsys media.aaudio
```

```
dumpsys media.audio_flinger
```

```
dumpsys media.audio_policy
```

```
dumpsys media_router
```

```
dumpsys media_session
```

```
dumpsys media.extractor
```

```
dumpsys media.metrics
```

```
dumpsys media.player
```

```
dumpsys media.radio
```

```
dumpsys media.resource_manager
```
