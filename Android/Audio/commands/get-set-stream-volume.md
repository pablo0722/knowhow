# Get/Set Stream Volume

## Android 10-

```vhdl
adb shell media volume --stream 3 --get
```

```
adb shell media volume --show --stream 3 --set 7
```

## Android 11+

```
adb shell cmd media_session volume --stream 3 --get
```

```
adb shell cmd media_session volume --show --stream 3 --set 7
```
