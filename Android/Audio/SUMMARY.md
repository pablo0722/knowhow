# Table of contents

## Commands

* [Diagnosis commands](README.md)
* [Get/Set Stream Volume](commands/get-set-stream-volume.md)

***

* [Information](information/README.md)
  * [Dynamic Audio Routing](information/dynamic-audio-routing.md)
  * [Audio Zone](information/audio-zone.md)
  * [Volume Group](information/volume-group.md)
  * [Audio Context](information/audio-context.md)
