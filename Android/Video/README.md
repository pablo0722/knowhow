---
description: Debug Video Commands
---

# Debug Video

## Dumpsys

Dumpsys diagnosis video commands

```sh
dumpsys display
```

```sh
dumpsys gfxinfo
```

```sh
dumpsys gpu
```

```sh
dumpsys graphicsstats
```

```sh
dumpsys media_projection
```

```sh
dumpsys SurfaceFlinger
```
