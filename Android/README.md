---
description: Add conditional compilation on Android.bp based android Project
---

# Conditional Compilation

We cannot add a conditional compilation on a `Android.bp` file. The way to do it is the following:

1.  Add a new library/binary on `Android.bp`:

    To achieve this we have 2 alternatives. For example, to implement a new feature `FEATURE_X` we can:

    * Option 1:
      1. Add a new file `foo_feat_x.cpp` including the new feature
      2.  Add new binary on `Android.bp` with the new source file:

          {% code title="Android.bp" lineNumbers="true" %}
          ```cpp
          cc_binary {
              name: "foo-service",
              srcs: ["foo.cpp"],
          }

          + cc_binary {
          +     name: "foo-feat-x-service",
          +     srcs: ["foo_feat_x.cpp"],
          + }
          ```
          {% endcode %}
    * Option 2:
      1.  Change original `foo.cpp` to include the new feature with `#if` or `#ifdef`:

          {% code title="foo.cpp" lineNumbers="true" %}
          ```cpp
          #ifdef ENABLE_FEATURE_X
              // ...
          #else
              // ...
          #endif
          ```
          {% endcode %}
      2.  Add new binary on `Android.bp` with extra define as compilation flag:

          {% code title="Android.bp" lineNumbers="true" %}
          ```cpp
          cc_binary {
              name: "foo-service",
              srcs: ["foo.cpp"],
          }

          + cc_binary {
          +     name: "foo-feat-x-service",
          +     srcs: ["foo.cpp"],
          +     cflags: ["-DENABLE_FEATURE_X"],
          + }
          ```
          {% endcode %}
2.  Then, add conditional compilation on `BoardConfig.mk` (or the corresponding `*.mk`) as usual:

    {% code title="BoardConfig.mk" lineNumbers="true" %}
    ```makefile
    ifeq ($(ENABLE_FEATURE_X),true)
        $(warning Including FEATURE_X on Foo-Service)
        PRODUCT_PACKAGES += foo-feat-x-service
    else
        $(warning Using Vanilla Foo-Service)
        PRODUCT_PACKAGES += foo-service
    endif
    ```
    {% endcode %}
3.  To compile, add export ENABLE\_FEATURE\_X=true before make command:

    {% code lineNumbers="true" %}
    ```sh
    export ENABLE_FEATURE_X=true && make -j$(nproc --all)
    ```
    {% endcode %}
