---
description: List installed and loaded modules
---

# List Modules

List loaded modules:

```sh
lsmod
```

List installed modules:

```sh
find /vendor_ramdisk/lib/modules/ /vendor_dlkm/lib/modules/ /system/lib/modules      -type f -name "*.ko" 2>/dev/null
```
