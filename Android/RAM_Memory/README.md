# RAM Memory commands

## BuddyInfo

Use the buddy algorithm:

```sh
cat /proc/buddyinfo
```

```sh
cat /proc/pagetypeinfo
```

Each column represents the number of pages of a certain order (a certain size) that are available at any given time.

For example, for output:

```sh
Node 0, zone      DMA     90      6      2
```

zone DMA (direct memory access), there are 90 of 2^0\*PAGE\_SIZE chunks of memory. Similarly, there are 6 of 2^1\*PAGE\_SIZE chunks, and 2 of 2^2\*PAGE\_SIZE chunks of memory available.

## LIBRANK

Ranks libraries by memory utilization (sharing)

```sh
librank
```

## ProCranck

Ranks processes by memory utilization

```sh
procrank
```

## MemTrack

Logs memory utilization to Android logs

```sh
memtrack
```

## MemInfo

Global memory statistics

```sh
cat /proc/meminfo
```

```sh
dumpsys meminfo
```

## VmStat

Kernel view of global memory, per variables

```sh
cat /proc/vmstat
```

## VmAllocInfo

Virtual memory allocation

```powershell
cat /proc/vmallocinfo
```

## ZoneInfo

Lists information about memory zones

```sh
cat /proc/zoneinfo
```
