# No Manifest Update Patch

Patch to add "--nmu" cli argument to skip manifest update

{% file src=".gitbook/assets/repo_nmu.patch" %}

Patch should be applied to `.repo/repo` directory with command:

```
git apply repo_nmu.patch
```

The use case is any situation where your manifest does not exist on server, but where you still want to do full sync for the projects, without having your workspace manifest switched to other branch or forwarded to latest or similar. This allows syncing to a historical manifest in git log, that does not have a branch, as well as when integrating something together that has not been pushed upstream yet. Changes can also exist locally on a manifest that is behind head, meaning not requiring rebase to latest.

Tested using:

{% code lineNumbers="true" %}
```
cd .repo/manifests/
git checkout <any hash 1>
// do local modifications
repo sync --no-manifest-update
```
{% endcode %}
