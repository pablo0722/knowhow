# Publish package Manually

## Command

To publish local file manually to package registry:

{% code overflow="wrap" %}
```sh
curl --header "PRIVATE-TOKEN: <user-access-token>" --upload-file "path/to/your/local/file" "https://gitlab.com/api/v4/projects/<id>/packages/generic/<package_name>/1.0/<path/to/file.txt>"
```
{% endcode %}

## Get project ID

To get your project id there are some methods:

1. From project's settings:
   1. Go to the project's main page
   2. On the Left Hand menu click Settings -> General
   3. There is a `Project ID` label next to the project name.
2. Search for `project_id` on projects' main page source code
3. Search on [all owned projects](api-rest/get.md#get-all-owned-projects) with API's curl command

## Example

Example to publish `hwlayer_test.apk` local file to project [https://gitlab.com/all\_projects885526/android\_projects/hw\_layers/](https://gitlab.com/all\_projects885526/android\_projects/hw\_layers/) (id 59292527) to package `hwlayer_test` with package version `1.0` and filename `hwlayer_test.apk`:

{% code overflow="wrap" %}
```sh
curl --header "PRIVATE-TOKEN: <user-access-token>" --upload-file "path/to/your/local/file" "https://gitlab.com/api/v4/projects/59292527/packages/generic/hwlayer_test/1.0/hwlayer_test.apk"
```
{% endcode %}
