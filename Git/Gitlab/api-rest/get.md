---
description: get commands for API REST with curl command
---

# GET

## Get all owned projects

{% code overflow="wrap" %}
```sh
curl -XGET --header "PRIVATE-TOKEN: <user_access_token>" "https://gitlab.com/api/v4/projects?owned=true"

```
{% endcode %}
