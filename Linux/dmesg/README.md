---
description: Filter kernel messages printed with `printk()`
---

# dmesg - Kernel messages

All [`printk()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html) messages are printed to the kernel log buffer is a ring buffer exported to userspace through `/dev/kmsg`.

The usual way to read it is using `dmesg` command

Usage is:

{% code title="kernel_module.c" lineNumbers="true" %}
```c
printk(KERN_INFO "Message: %s\n", arg);
```
{% endcode %}

The available log levels are:

| Name          | String | Alias function                                                                                                                                                                                                    |
| ------------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| KERN\_EMERG   | “0”    | [`pr_emerg()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_emerg)                                                                                                                      |
| KERN\_ALERT   | “1”    | [`pr_alert()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_alert)                                                                                                                      |
| KERN\_CRIT    | “2”    | [`pr_crit()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_crit)                                                                                                                        |
| KERN\_ERR     | “3”    | [`pr_err()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_err)                                                                                                                          |
| KERN\_WARNING | “4”    | [`pr_warn()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_warn)                                                                                                                        |
| KERN\_NOTICE  | “5”    | [`pr_notice()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_notice)                                                                                                                    |
| KERN\_INFO    | “6”    | [`pr_info()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_info)                                                                                                                        |
| KERN\_DEBUG   | “7”    | [`pr_debug()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_debug) and [`pr_devel()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_devel) if DEBUG is defined |
| KERN\_DEFAULT | “”     |                                                                                                                                                                                                                   |
| KERN\_CONT    | “c”    | [`pr_cont()`](https://www.kernel.org/doc/html/next/core-api/printk-basics.html#c.pr\_cont)                                                                                                                        |

You can check the current console\_loglevel with:

{% code title="as user" %}
```sh
cat /proc/sys/kernel/printk
4        4        1        7
```
{% endcode %}

The result shows the _current_, _default_, _minimum_ and _boot-time-default_ log levels.

To change the current console\_loglevel simply write the desired level to `/proc/sys/kernel/printk`. For example, to print all messages to the console:

{% code title="as root" %}
```sh
echo 8 > /proc/sys/kernel/printk
```
{% endcode %}

Another way, using `dmesg`:

{% code title="as root" %}
```sh
dmesg -n 5
```
{% endcode %}
