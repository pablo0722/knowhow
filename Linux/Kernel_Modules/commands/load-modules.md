---
description: Load installed modules
---

# Load Modules

## Load module at Runtime

`insmod` simply add a module

```sh
insmod /path/to/mydrv.ko
```

`modprobe` is the intelligent version of `insmod`. Looks \*_only in the standard installed module directories\*_ for any dependency (if that particular module is dependent on any other module) and loads them. It parses the file `modules.dep` in order to load dependencies first, prior to loading the given module

```
modprobe mydrv
```

## Load module at Boottime

If you want some module to be loaded at boot time, just create the file `/etc/modules- load.d/*.conf`, and add the module's name that should be loaded, one per line.

Each .conf file should be meaningful to you. People usually use `/etc/modules-load.d/modules.conf`. You may create as many .conf files as you need.
