---
description: List installed and loaded modules
---

# List Modules

List loaded modules:

```sh
lsmod
```

List installed modules on Linux:

```sh
find /usr/lib/modules/ -type f -name "*.ko" 2>/dev/null
```
