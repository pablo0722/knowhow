# Download Link

* Download Linux CrossCompilers: [https://developer.arm.com/downloads/-/gnu-a](https://developer.arm.com/downloads/-/gnu-a)
* Unzip on known directory, e.g.: /`opt/`
* Set env variable: `export AARCH64_GCC_CROSS_COMPILE=<crosscompiler_directory>/bin/<target>-`
  * e.g.: for crosscompiler `gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu` use target: `aarch64-none-linux-gnu`, so env variable should be `export AARCH64_GCC_CROSS_COMPILE=/opt/``gcc-arm-10.3-2021.07-x86_64-aarch64-none-linux-gnu/bin/aarch64-none-linux-gnu-`
