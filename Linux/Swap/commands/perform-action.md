# Perform Action

## Disable swap partition

```
swapoff /dev/block/mmcblk0p20
```

## Enable swap partition

```
mkswap /dev/block/mmcblk0p20 && swapon /dev/block/mmcblk0p20
```
