---
description: List environment variables
---

# List Environment

Print environment variables

```sh
printenv
```

