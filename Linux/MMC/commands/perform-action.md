# Perform Action

### Drop the page cache and/or inode and dentry caches

* To free pagecache echo “1”
* To free dentries and inodes echo “2”
* To free pagecache, dentries and inodes echo “3”

```
echo 3 > /proc/sys/vm/drop_caches
```
