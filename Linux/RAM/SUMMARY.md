# Table of contents

## Commands

* [Retrieve information](README.md)
* [Perform benchmark](commands/perform-benchmark.md)
* [Perform action](commands/perform-action.md)

## Information

* [Memory zones](information/memory-zones.md)
* [More Information](information/more-information.md)
