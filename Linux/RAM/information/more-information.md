# More Information

**/proc/\* files:**

* [![](https://mjmwired.net/favicon.ico)Linux Kernel Documentation / filesystems / proc.txt](https://mjmwired.net/kernel/Documentation/filesystems/proc.txt)
* [https://www.kernel.org/doc/Documentation/filesystems/proc.txt](https://www.kernel.org/doc/Documentation/filesystems/proc.txt)

**/proc/meminfo:**[![](https://www.baeldung.com/wp-content/themes/baeldung/favicon/linux/favicon-16x16.png)The /proc/meminfo File in Linux | Baeldung on Linux](https://www.baeldung.com/linux/proc-meminfo)

**OOM:** [![](https://gist.github.com/favicon.ico)Notes taken while looking into memory issues on low-RAM devices](https://gist.github.com/flyfire/4cd0f240e8f55521beba)

**Memory zones:** [![](https://litux.nl/favicon.ico)Zones](https://litux.nl/mirror/kerneldevelopment/0672327201/ch11lev1sec2.html)

**Swap:** [![](https://linuxhint.com/wp-content/uploads/2021/09/cropped-512x512\_linuxhint-32x32.png)Understanding vm.swappiness](https://linuxhint.com/understanding\_vm\_swappiness/)

**Physical memory:** [Describing Physical Memory](https://www.kernel.org/doc/gorman/html/understand/understand005.html)

**I/O scheduling types:** [![](https://www.cloudbees.com/assets/favicon-16x16.png)Improving Linux System Performance with I/O Scheduler Tuning](https://www.cloudbees.com/blog/linux-io-scheduler-tuning)

**queue sysfs:** [https://www.kernel.org/doc/Documentation/block/queue-sysfs.txt](https://www.kernel.org/doc/Documentation/block/queue-sysfs.txt)
