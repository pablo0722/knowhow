# Memory zones

Because of hardware limitations, the kernel cannot treat all pages as identical. Some pages, because of their physical address in memory, cannot be used for certain tasks. Because of this limitation, the kernel divides pages into different _zones_. The kernel uses the zones to group pages of similar properties. In particular, Linux has to deal with two shortcomings of hardware with respect to memory addressing:

* Some hardware devices are capable of performing DMA (direct memory access) to only certain memory addresses.
* Some architectures are capable of physically addressing larger amounts of memory than they can virtually address. Consequently, some memory is not permanently mapped into the kernel address space.[high memory](https://litux.nl/mirror/kerneldevelopment/0672327201/ch11lev1sec9.html#ch11lev1sec9)," which are pages not permanently mapped into the kernel's address space.

Because of these constraints, there are three memory zones in Linux:

* **ZONE\_DMA:** This zone contains pages that are capable of undergoing DMA.[high memory](https://litux.nl/mirror/kerneldevelopment/0672327201/ch11lev1sec9.html#ch11lev1sec9)," which are pages not permanently mapped into the kernel's address space. Is the low 16 MBytes of memory. At this point it exists for historical reasons; once upon what is now a long time ago, there was hardware that could only do DMA into this area of physical memory.
* **ZONE\_NORMAL:** This zone contains normal, regularly mapped, pages. Memory directly accessible by the kernel.
* **ZONE\_HIGHMEM:** This zone contains "[high memory](https://litux.nl/mirror/kerneldevelopment/0672327201/ch11lev1sec9.html#ch11lev1sec9)," which are pages not permanently mapped into the kernel's address space. Dynamically mapped pages. Is all RAM above 896 MB, including RAM above 4 GB on sufficiently large machines.
