# Retrieve information

### Print the partition used as swap

```
cat /proc/swaps
```

### Memory zones statistics

```
cat /proc/zoneinfo
```

### Usage report about memory on the system

```
cat /proc/meminfo
```
