# List Open Files

Listar puertos abiertos

```bash
lsof -n -i
```

Listar los ficheros que el usuario `root` mantiene abiertos actualmente.

```bash
lsof -u root
```
