# Implementation

## Information

To do an ALSA-compatible driver you must define the following things:

### Widgets definition

You must define internal structure of the integrated-circuit using the DAMP Widgets Definitions.

I.e.: You must define the internal components like ADC, DAC, inputs, outputs, mixers, muxes, etc.



### Widget Interconnections

Widgets are connected to each other within the codec, platform and machine by audio paths (called interconnections). Each interconnection must be defined in order to create a map of all audio paths between widgets.

## Reference

* Explanation of DAPM widgets; [https://www.kernel.org/doc/html/v4.14/sound/soc/dapm.html](https://www.kernel.org/doc/html/v4.14/sound/soc/dapm.html)
* List of methods to define DAPM widgets:  [https://www.alsa-project.org/wiki/DAPM#SND\_SOC\_DAPM\_MIXER(wname,\_wreg,\_wshift,\_winvert,\_wcontrols,\_wncontrols)](https://www.alsa-project.org/wiki/DAPM#SND\_SOC\_DAPM\_MIXER\(wname,\_wreg,\_wshift,\_winvert,\_wcontrols,\_wncontrols\))
